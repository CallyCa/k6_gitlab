import http from "k6/http";
import { check, fail, sleep } from "k6";


export let options = {
  iterations: 4,
  thresholds: {
    http_req_duration: ["p(99)<1400", "p(50)<2300"],
    http_req_waiting: ["avg<3000"],
    http_req_failed: ["rate<0.75"]
  },
};


export default function() {
    // Send a JSON encoded POST request
    let body = JSON.stringify({
    pagador: {
        ispb: "0",
        tpPessoa: "0",
        cpfCnpj: "47025001040",
        nrAgencia: "0001",
        tpConta: "0",
        nrConta: "123",
        nome: "teste validar"
    },
    recebedor: {
        ispb: "13884775",
        tpPessoa: "0",
        cpfCnpj: "48314768030",
        nrAgencia: "0001",
        tpConta: "3",
        nrConta: "265501850"
    },
    valor: "1.23",
    infEntreClientes: "teste validar",
    creditoOrdemPagamento: {
        endToEndId: "181387b7-8965-4660-914c-15e93f20e1ff"
    }
});
    let res = http.post("http://", body, { headers: { "Content-Type": "application/json" }});
    
    console.log('Response time was ' + String(res.timings.duration) + ' ms');
    
    // Use JSON.parse to deserialize the JSON (instead of using the r.json() method)
    let j = JSON.parse(res.body);
    
    
    // Verify response
    if (
      !check(res, {
        "status is 200": (r) => r.status === 200,
      })
    )  
      {
      fail(res, {
        "status is 500": (r) => r.status === 500,
      });
    }  
    
    sleep(Math.random() * 30);
}




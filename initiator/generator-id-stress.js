import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
      { target: 800, duration: "30s" },
      { target: 800, duration: "4m" },
      { target: 0, duration: "30s" }
  ],
  thresholds: {
      // We want the 90th percentile of all HTTP request durations to be less than 8s
      "http_req_duration": ["p(90)<10000"],
      // Thresholds based on the custom metric we defined and use to track application failures
      "check_failure_rate": [
          // Global failure rate should be less than 10%
          "rate<0.10",
          // Abort the test early if it climbs over 15%
          { threshold: "rate<=0.15", abortOnFail: true },
      ],
  },
};

export default function() {  
  const url = 'http:/';
  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let body = JSON.stringify({
    instanceId : 'k6'
  });
  const res = http.post(url, body, params);
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
};


import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
// import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";


export function handleSummary(data) {
    return {
      'stdout': textSummary(data, { indent: ' ', enableColors: true }), // Show the text summary to stdout...
      //'initiator/junit.xml': jUnit(data), // but also transform it and save it as a JUnit XML...
      'initiator/summary.json': JSON.stringify(data), // and a JSON with all the details...
      'initiator/result_generator_id_stress.html': htmlReport(data),
      // And any other JS transformation of the data you can think of,
      // you can write your own JS helpers to transform the summary data however you like!
    };
}
  